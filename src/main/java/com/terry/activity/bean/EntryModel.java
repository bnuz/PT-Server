package com.terry.activity.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.ArrayList;

/**
 * Create On 18/4/25.
 *
 * @author TerrySolar
 */
@Entity
@Table(name = "pt_entry")
public class EntryModel extends BaseModel {

    private EnrollModel enrollModel;

    private ActivityModel activityModel;

    private CatalogModel catalogModel;

    private Long voteDDL;

    private Long entry_no;

    private String imgUrl;

    private String title;

    private String brief;

    @ManyToOne(
            targetEntity = EnrollModel.class,
            cascade = CascadeType.MERGE,
            fetch = FetchType.EAGER
    )
    @JoinColumn(name = "enroll_id", referencedColumnName = "id")
    public EnrollModel getEnrollModel() {
        return enrollModel;
    }

    public void setEnrollModel(EnrollModel enrollModel) {
        this.enrollModel = enrollModel;
    }


    @ManyToOne(
            targetEntity = ActivityModel.class,
            cascade = CascadeType.MERGE,
            fetch = FetchType.LAZY
    )
    @JoinColumn(name = "activity_id", referencedColumnName = "id")
    public ActivityModel getActivityModel() {
        return activityModel;
    }

    public void setActivityModel(ActivityModel activityModel) {
        this.activityModel = activityModel;
    }

    @ManyToOne(
            targetEntity = CatalogModel.class,
            cascade = CascadeType.MERGE,
            fetch = FetchType.LAZY
    )
    @JoinColumn(name = "catalog_id", referencedColumnName = "id")
    public CatalogModel getCatalogModel() {
        return catalogModel;
    }

    public void setCatalogModel(CatalogModel catalogModel) {
        this.catalogModel = catalogModel;
    }

    public Long getVoteDDL() {
        return voteDDL;
    }

    public void setVoteDDL(Long voteDDL) {
        this.voteDDL = voteDDL;
    }

    public Long getEntry_no() {
        return entry_no;
    }

    public void setEntry_no(Long entry_no) {
        this.entry_no = entry_no;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }
}
