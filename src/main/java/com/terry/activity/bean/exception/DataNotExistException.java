package com.terry.activity.bean.exception;

/**
 * Create On 18/4/25.
 *
 * @author TerrySolar
 */
public class DataNotExistException extends Exception {
    public DataNotExistException() {
    }

    public DataNotExistException(String message) {
        super(message);
    }
}
