package com.terry.activity.bean.exception;

import javax.servlet.ServletException;

/**
 * Create On 18/4/22.
 *
 * @author TerrySolar
 */
public class WechatAuthException extends ServletException {

    public WechatAuthException() {
    }

    public WechatAuthException(String message) {
        super(message);
    }

}
