package com.terry.activity.bean.exception;

import com.terry.activity.bean.constant.ErrorCode;
import com.terry.activity.bean.message.ResponseMsg;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.criteria.ParameterExpression;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * 统一异常处理
 *
 * Create On 17/11/7.
 *
 * @author TerrySolar
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private static Logger logger = Logger.getLogger(GlobalExceptionHandler.class) ;

    @ExceptionHandler(value = WechatAuthException.class)
    @ResponseBody
    public ResponseMsg wechatAuthExceptionHandler(HttpServletResponse response, Exception exception) {
        return new ResponseMsg(ErrorCode.WECHAT_NO_LOGIN, "need wechat auth");
    }

    @ExceptionHandler(value = DataAlreadyExistException.class)
    @ResponseBody
    public ResponseMsg dataAlreadyExistExceptionHandler(HttpServletResponse response, Exception exception) {
        return new ResponseMsg(ErrorCode.DATA_ALREADY_EXIST, exception.getMessage());
    }

    @ExceptionHandler(value = DataNotExistException.class)
    @ResponseBody
    public ResponseMsg dataNotExistExceptionHandler(HttpServletResponse response, Exception exception) {
        return new ResponseMsg(ErrorCode.DATA_NOT_EXIST, exception.getMessage());
    }

    @ExceptionHandler(value = ParameterException.class)
    @ResponseBody
    public ResponseMsg parameterExceptionHandler(HttpServletResponse response, Exception exception) {
        return new ResponseMsg(ErrorCode.PARAM_ERROR, exception.getMessage());
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResponseMsg commonExceptionHandler(HttpServletResponse response, Exception exception) {
        exception.printStackTrace();
        return new ResponseMsg(ErrorCode.SERVER_ERROR, "server error");
    }





}
