package com.terry.activity.bean.exception;

/**
 * Create On 18/4/25.
 *
 * @author TerrySolar
 */
public class DataAlreadyExistException extends Exception {
    public DataAlreadyExistException() {
    }

    public DataAlreadyExistException(String message) {
        super(message);
    }
}
