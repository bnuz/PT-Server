package com.terry.activity.bean.exception;

/**
 * 参数异常
 *
 * Create On 18/6/17.
 *
 * @author TerrySolar
 */
public class ParameterException extends Exception {

    public ParameterException() {
    }

    public ParameterException(String message) {
        super(message);
    }
}
