package com.terry.activity.bean;

import javax.persistence.*;

/**
 * Create On 18/6/18.
 *
 * @author TerrySolar
 */
@Entity
@Table(name = "pt_catalog")
public class CatalogModel extends BaseModel {

    private String name;

    private String imgUrl;

    private ActivityModel activityModel;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    @ManyToOne(
            targetEntity = ActivityModel.class,
            cascade = CascadeType.MERGE,
            fetch = FetchType.LAZY
    )
    @JoinColumn(name = "activity_id", referencedColumnName = "id")
    public ActivityModel getActivityModel() {
        return activityModel;
    }

    public void setActivityModel(ActivityModel activityModel) {
        this.activityModel = activityModel;
    }

    @Override
    public String toString() {
        return "CatalogModel{" +
                "name='" + name + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", id=" + id +
                ", updateTime=" + updateTime +
                ", createTime=" + createTime +
                '}';
    }
}
