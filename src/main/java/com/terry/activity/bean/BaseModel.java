package com.terry.activity.bean;



import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Create On 17/9/25.
 *
 * @author TerrySolar
 */
@MappedSuperclass
public class BaseModel implements Serializable{

    protected long id;
    protected Date updateTime;
    protected Date createTime;


    public BaseModel() {
    }

    public BaseModel(long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "update_time")
    @UpdateTimestamp
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Column(name = "create_time")
    @CreationTimestamp
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
