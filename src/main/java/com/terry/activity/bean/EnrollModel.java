package com.terry.activity.bean;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Create On 18/4/22.
 *
 * @author TerrySolar
 */
@Entity
@Table(name = "pt_enroll")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EnrollModel extends BaseModel {

    private String author;

    private String email;

    private String intro;

    private String phone;

    private String imgHashJson;

    private ArrayList<String> imgHash;

    private Integer sex;

    private String openId;

    private Boolean isEntry;


    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImgHashJson() {
        return imgHashJson;
    }

    public void setImgHashJson(String imgHashJson) {
        this.imgHashJson = imgHashJson;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public Boolean getEntry() {
        return isEntry;
    }

    public void setEntry(Boolean entry) {
        isEntry = entry;
    }

    public ArrayList<String> getImgHash() {
        if (imgHash == null && imgHashJson != null) {
            imgHash = new ArrayList<>(Arrays.asList(imgHashJson.split(",")));
        }

        return imgHash;
    }

    public void setImgHash(ArrayList<String> imgHash) {
        this.imgHash = imgHash;
    }

    @Override
    public String toString() {
        return "EnrollModel{" +
                "author='" + author + '\'' +
                ", email='" + email + '\'' +
                ", intro='" + intro + '\'' +
                ", phone='" + phone + '\'' +
                ", imgHashJson='" + imgHashJson + '\'' +
                ", imgHash=" + imgHash +
                ", sex=" + sex +
                ", openId='" + openId + '\'' +
                ", isEntry=" + isEntry +
                ", id=" + id +
                ", updateTime=" + updateTime +
                ", createTime=" + createTime +
                '}';
    }
}
