package com.terry.activity.bean.message;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.sql.Time;
import java.util.Date;

/**
 * 响应消息实体
 *
 * Create On 17/11/7.
 *
 * @author TerrySolar
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseMsg {
    @JSONField(name = "errorcode")
    private Integer errorCode;

    private String message;

    private Long timestamp;

    public ResponseMsg(Integer errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
        this.timestamp = System.currentTimeMillis();
    }

    public ResponseMsg(Integer errorCode, String message, Long timestamp) {
        this.errorCode = errorCode;
        this.message = message;
        this.timestamp = timestamp;
    }

    public ResponseMsg() {
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}
