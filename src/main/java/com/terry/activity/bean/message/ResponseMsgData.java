package com.terry.activity.bean.message;

import java.util.List;

/**
 * Create On 18/4/26.
 *
 * @author TerrySolar
 */
public class ResponseMsgData extends ResponseMsg {

    private Object data;

    public ResponseMsgData() {
    }

    public ResponseMsgData(List data) {
        this.data = data;
    }

    public ResponseMsgData(Integer errorCode, String message, Object data) {
        super(errorCode, message);
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
