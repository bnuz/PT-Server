package com.terry.activity.bean.message;

import com.alibaba.fastjson.JSONObject;

import java.util.Date;

/**
 * Create On 17/11/8.
 *
 * @author TerrySolar
 */
public class ResponseMsgJson extends ResponseMsg {
    private JSONObject data;

    public ResponseMsgJson() {
    }

    public ResponseMsgJson(Integer errorCode, String message, JSONObject data) {
        super(errorCode, message);
        this.data = data;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }
}
