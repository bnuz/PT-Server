package com.terry.activity.bean.message;


/**
 * Create On 17/11/11.
 *
 * @author TerrySolar
 */
public class ResponseMsgRedirect extends ResponseMsg {

    private String redirect;

    public ResponseMsgRedirect(Integer errorCode, String message, Long timestamp, String redirect) {
        super(errorCode, message, timestamp);
        this.redirect = redirect;
    }

    public ResponseMsgRedirect(String redirect) {
        this.redirect = redirect;
    }

    public ResponseMsgRedirect() {
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }
}
