package com.terry.activity.bean.message;

import com.alibaba.fastjson.JSONArray;
import com.terry.activity.bean.BaseModel;

import java.util.List;

/**
 * Create On 18/4/26.
 *
 * @author TerrySolar
 */
public class ResponseMsgJsonArray extends ResponseMsg {

    private JSONArray data;

    public ResponseMsgJsonArray() {
    }

    public ResponseMsgJsonArray(Integer errorCode, String message, JSONArray data) {
        super(errorCode, message);
        this.data = data;
    }

    public List getData() {
        return data;
    }

    public void setData(JSONArray data) {
        this.data = data;
    }
}
