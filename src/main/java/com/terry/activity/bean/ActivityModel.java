package com.terry.activity.bean;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Create On 18/4/24.
 *
 * @author TerrySolar
 */
@Entity
@Table(name = "pt_activity")
public class ActivityModel extends BaseModel{

    private String title;

    private String brief;

    private String coverImgUrl;

    private Long voteStartTime;

    private Long voteEndTime;

    private int type;

    private Boolean isEnable;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getCoverImgUrl() {
        return coverImgUrl;
    }

    public void setCoverImgUrl(String coverImgUrl) {
        this.coverImgUrl = coverImgUrl;
    }

    public Long getVoteStartTime() {
        return voteStartTime;
    }

    public void setVoteStartTime(Long voteStartTime) {
        this.voteStartTime = voteStartTime;
    }

    public Long getVoteEndTime() {
        return voteEndTime;
    }

    public void setVoteEndTime(Long voteEndTime) {
        this.voteEndTime = voteEndTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Boolean getEnable() {
        return isEnable;
    }

    public void setEnable(Boolean enable) {
        isEnable = enable;
    }
}
