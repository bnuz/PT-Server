package com.terry.activity.bean.constant;

/**
 * Create On 18/4/19.
 *
 * @author TerrySolar
 */
public class ErrorCode {

    /** Acces OK */
    public static final int OK = 0;

    /** internal server error */
    public static final int SERVER_ERROR = -1;

    /** Data Alreay Exist */
    public static final int DATA_ALREADY_EXIST = -2;

    /** Data Not Exist */
    public static final int DATA_NOT_EXIST = -3;

    /** parameter error */
    public static final int PARAM_ERROR = -4;

    /** Wechat No Login */
    public static final int WECHAT_NO_LOGIN = 10001;

    /** No Enroll Record */
    public static final int ENROLL_NO_RECORD = 20001;

    /** Enroll Already Exist */
    public static final int ENROLL_ALREADY_EXIST = 20002;

    /** vote already exist */
    public static final int VOTE_ALREADY_EXIST = 30001;

    /** vote time error */
    public static final int VOTE_TIME_ERROR = 30002;

    /** entry not exist */
    public static final int ENTRY_NOT_EXIST = 40001;






    /** admin add entry error */
    public static final int ADMIN_ADD_ENTRY_ERROR = 91001;

}
