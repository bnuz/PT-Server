package com.terry.activity.bean.constant;

/**
 * Create On 18/6/17.
 *
 * @author TerrySolar
 */
public class ActivityType {

    /** normal activity type */
    public static final int NORMAL = 1;

    /** add content manually */
    public static final int MANUAL = 2;

    /** add content manually with catalog */
    public static final int MANUAL_CATALOG = 3;

}
