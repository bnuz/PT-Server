package com.terry.activity.bean;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;

/**
 * Create On 18/4/25.
 *
 * @author TerrySolar
 */
@Entity
@Table(name = "pt_vote")
public class VoteModel extends BaseModel{


    private EntryModel entryModel;

    private ActivityModel activityModel;

    private CatalogModel catalogModel;

    private String openId;


    @ManyToOne(
            targetEntity = EntryModel.class,
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    @JoinColumn(name = "entry_id", referencedColumnName = "id")
    public EntryModel getEntryModel() {
        return entryModel;
    }

    public void setEntryModel(EntryModel entryModel) {
        this.entryModel = entryModel;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }


    @ManyToOne(
            targetEntity = ActivityModel.class,
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    @JoinColumn(name = "activity_id", referencedColumnName = "id")
    public ActivityModel getActivityModel() {
        return activityModel;
    }

    public void setActivityModel(ActivityModel activityModel) {
        this.activityModel = activityModel;
    }

    @ManyToOne(
            targetEntity = CatalogModel.class,
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    @JoinColumn(name = "catalog_id", referencedColumnName = "id")
    public CatalogModel getCatalogModel() {
        return catalogModel;
    }

    public void setCatalogModel(CatalogModel catalogModel) {
        this.catalogModel = catalogModel;
    }

    @Override
    public String toString() {
        return "VoteModel{" +
                "entryModel=" + entryModel +
                ", activityModel=" + activityModel +
                ", openId='" + openId + '\'' +
                ", id=" + id +
                ", updateTime=" + updateTime +
                ", createTime=" + createTime +
                '}';
    }
}
