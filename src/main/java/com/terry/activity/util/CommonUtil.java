package com.terry.activity.util;

import com.terry.activity.bean.constant.CC;

/**
 * Create On 18/4/25.
 *
 * @author TerrySolar
 */
public class CommonUtil {



    /**
     * 生成redis key （prefix + key）
     *
     * @param key 键值
     * @return
     */
    public static String GenerateRedisKey(String key) {
        return CC.REDIS_PREFIX + key;
    }
}
