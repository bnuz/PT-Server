package com.terry.activity.service;

import com.terry.activity.bean.CatalogModel;

import java.util.List;

/**
 * Create On 18/6/18.
 *
 * @author TerrySolar
 */
public interface ICatalogService {

    List<CatalogModel> retrieveCatalog();

    List<CatalogModel> retrieveCatalog(Long activityId);

    CatalogModel createCatalog(CatalogModel catalogModel);

    void deleteCatalog(Long catalogId);

}
