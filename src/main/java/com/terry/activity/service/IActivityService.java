package com.terry.activity.service;

import com.terry.activity.bean.ActivityModel;
import com.terry.activity.bean.exception.DataNotExistException;

import java.util.List;

/**
 *
 * Create On 18/6/17.
 *
 * @author TerrySolar
 */
public interface IActivityService {

    /**
     * 设置活动投票时间
     *
     * @param activityId 活动ID
     * @param startTime 开始时间 yyyy-MM-dd
     * @param endTime 结束时间 yyyy-MM-dd
     * @return
     * @throws DataNotExistException 活动不存在异常
     */
    ActivityModel setVoteTimeRange(Long activityId, Long startTime, Long endTime) throws DataNotExistException;

    ActivityModel getActivity(Long activityId) throws DataNotExistException;

    List<ActivityModel> getActivity();

    ActivityModel updateActivity(ActivityModel activityModel) throws DataNotExistException;

    ActivityModel addActivity(ActivityModel activityModel);
}
