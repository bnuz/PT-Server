package com.terry.activity.service;

import com.terry.activity.bean.EntryModel;
import com.terry.activity.bean.exception.DataAlreadyExistException;
import com.terry.activity.bean.exception.DataNotExistException;
import com.terry.activity.bean.exception.ParameterException;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Create On 18/4/26.
 *
 * @author TerrySolar
 */
public interface IEntryService {


    /**
     * 根据活动ID查找所有入选作品
     *
     * @param activityId 活动ID
     * @return
     */
    List<EntryModel> retrieveAllEntryOfActivity(Long activityId);


    /**
     * 根据活动ID和分类ID查找入选作品
     *
     * @param activityId 活动ID
     * @param catalogId 分类ID
     * @return
     */
    List<EntryModel> retrieveEntryOfActivityAndCatalog(Long activityId, Long catalogId);

    /**
     * 新增入围作品
     *
     * @param activityId 活动ID
     * @param enrollId 报名ID
     * @return EntryModel
     */
    @Transactional
    EntryModel addEntry(Long activityId, Long enrollId) throws DataAlreadyExistException, DataNotExistException;


    EntryModel addManualEntry(EntryModel entryModel) throws ParameterException, DataNotExistException;


    @Transactional
    Boolean cancelEntry(Long activityId, Long enrollId) throws DataNotExistException;


    Boolean deleteEntry(Long entryId);

}
