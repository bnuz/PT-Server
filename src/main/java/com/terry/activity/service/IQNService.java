package com.terry.activity.service;

/**
 * 七牛相关服务接口
 *
 * Create On 18/4/20.
 *
 * @author TerrySolar
 */
public interface IQNService {


    /**
     * 构造上传Token
     *
     * @return Upload Token
     */
    String createUploadToken();

    /**
     * 构造上传Token
     *
     * @param accessKey 七牛 accessKey
     * @param secretKey 七牛 secretKey
     * @param bucket 七牛空间名称
     * @return Upload Token
     */
    String createUploadToken(String accessKey, String secretKey, String bucket);

}
