package com.terry.activity.service;

import com.terry.activity.bean.EnrollModel;
import com.terry.activity.bean.exception.DataAlreadyExistException;

import java.util.List;

/**
 * Create On 18/4/22.
 *
 * @author TerrySolar
 */
public interface IEnrollService {

    /**
     * 写入报名数据
     *
     * @param enrollModel EnrollModel
     * @return boolean 是否报名成功
     */
    boolean enroll(EnrollModel enrollModel) throws DataAlreadyExistException;


    /**
     * 根据openid获取报名信息
     *
     * @param openId 微信openid
     * @return EnrollModel
     */
    EnrollModel retrieveEnroll(String openId);


    /**
     * 分页查询报名信息
     *
     * @param page 页码
     * @param size 每页数量
     * @return List<EnrollModel>
     */
    List<EnrollModel> retrieveEnroll(Integer page, Integer size);




}
