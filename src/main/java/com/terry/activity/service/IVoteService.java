package com.terry.activity.service;

import com.terry.activity.bean.VoteModel;
import com.terry.activity.bean.exception.DataAlreadyExistException;
import com.terry.activity.bean.exception.DataNotExistException;
import org.springframework.data.domain.Example;

/**
 * Create On 18/4/25.
 *
 * @author TerrySolar
 */
public interface IVoteService {

    /**
     * 投票
     *
     * @param entryId 入选作品
     * @param openId 微信openId
     * @return 是否投票成功
     * @throws DataAlreadyExistException 重复投票抛出异常
     */
    boolean vote(Long entryId, String openId) throws DataAlreadyExistException, DataNotExistException;


    /**
     * 获取用户在指定活动中的投票记录
     *
     * @param openId 微信openId
     * @param activityId 活动ID
     * @return VoteModel
     */
    VoteModel retrieveVote(String openId, Long activityId);


    /**
     * 获取用户在指定活动和分类中的投票记录
     *
     * @param openId 微信openID
     * @param activityId 活动ID
     * @param catalogId 分类ID
     * @return VoteModel
     */
    VoteModel retrieveVote(String openId, Long activityId, Long catalogId);


    /**
     * 获取entry对应的票数
     *
     * @param entryId 入选作品的ID
     * @return Integer 作品的票数
     */
    Integer retrieveVoteCount(Long entryId);




}
