package com.terry.activity.service.impl;

import com.terry.activity.bean.ActivityModel;
import com.terry.activity.bean.EnrollModel;
import com.terry.activity.bean.EntryModel;
import com.terry.activity.bean.exception.DataAlreadyExistException;
import com.terry.activity.bean.exception.DataNotExistException;
import com.terry.activity.bean.exception.ParameterException;
import com.terry.activity.dao.IActivityDao;
import com.terry.activity.dao.ICatalogDao;
import com.terry.activity.dao.IEnrollDao;
import com.terry.activity.dao.IEntryDao;
import com.terry.activity.service.IEntryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Create On 18/4/26.
 *
 * @author TerrySolar
 */
@Service("entryService")
public class EntryServiceImpl implements IEntryService {

    @Resource
    private IEntryDao entryDao;

    @Resource
    private IEnrollDao enrollDao;

    @Resource
    private IActivityDao activityDao;

    @Resource
    private ICatalogDao catalogDao;

    @Override
    public List<EntryModel> retrieveAllEntryOfActivity(Long activityId) {
        return this.entryDao.findAllByActivityModel_Id(activityId);
    }

    @Override
    public List<EntryModel> retrieveEntryOfActivityAndCatalog(Long activityId, Long catalogId) {
        return this.entryDao.findByActivityModel_IdAndCatalogModel_Id(activityId, catalogId);
    }

    @Override
    public EntryModel addEntry(Long activityId, Long enrollId) throws DataAlreadyExistException, DataNotExistException {

        EnrollModel enrollModel;

        try {
            enrollModel = enrollDao.findById(enrollId).get();
        } catch (NoSuchElementException e) {
            return null;
        }

        if (enrollModel.getEntry()) throw new DataAlreadyExistException("请勿重复操作！");

        ActivityModel activityModel;
        try {
            activityModel = this.activityDao.findById(activityId).get();
        } catch (NoSuchElementException e) {
            throw new DataNotExistException("活动不存在！");
        }

        // 添加入围记录
        EntryModel entryModel = new EntryModel();
        entryModel.setActivityModel(activityModel);
        entryModel.setEnrollModel(enrollModel);

        // 更新报名记录中的是否入围字段
        enrollModel.setEntry(true);
        enrollDao.save(enrollModel);

        return entryDao.save(entryModel);
    }

    @Override
    public EntryModel addManualEntry(EntryModel entryModel) throws ParameterException, DataNotExistException {

        if (entryModel.getActivityModel() == null) {
            throw new ParameterException("参数错误");
        }

        try {
            this.activityDao.findById(entryModel.getActivityModel().getId()).get();
        } catch (NoSuchElementException e) {
            throw new DataNotExistException("活动不存在！");
        }

        return this.entryDao.save(entryModel);
    }

    @Override
    public Boolean cancelEntry(Long activityId, Long enrollId) throws DataNotExistException {

        EnrollModel enrollModel;

        try {
            // 更新报名信息
            enrollModel = this.enrollDao.findById(enrollId).get();
        } catch (NoSuchElementException e) {
            throw new DataNotExistException("报名数据不存在！");
        }

        enrollModel.setEntry(false);

        EntryModel entryModel = this.entryDao.findEntryModelByActivityModel_IdAndEnrollModel_Id(activityId, enrollId);
        if (entryModel == null) {
            throw new DataNotExistException("入围数据不存在！");
        }
        this.enrollDao.save(enrollModel);
        this.entryDao.delete(entryModel);

        return true;
    }

    @Override
    public Boolean deleteEntry(Long entryId) {
        try {
            this.entryDao.deleteById(entryId);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
