package com.terry.activity.service.impl;

import com.qiniu.util.Auth;
import com.terry.activity.bean.constant.CC;
import com.terry.activity.service.IQNService;
import org.springframework.stereotype.Service;

/**
 * Create On 18/4/20.
 *
 * @author TerrySolar
 */
@Service("QNService")
public class QNServiceImpl implements IQNService {


    @Override
    public String createUploadToken() {
        Auth auth = Auth.create(CC.QINIU_ACCESS_KEY, CC.QINIU_SECRET_KEY);
        return auth.uploadToken(CC.QINIU_BUCKET);
    }

    @Override
    public String createUploadToken(String accessKey, String secretKey, String bucket) {
        Auth auth = Auth.create(accessKey, secretKey);
        return auth.uploadToken(bucket);
    }


}
