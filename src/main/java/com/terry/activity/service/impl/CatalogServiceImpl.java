package com.terry.activity.service.impl;

import com.terry.activity.bean.CatalogModel;
import com.terry.activity.dao.ICatalogDao;
import com.terry.activity.service.ICatalogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Create On 18/6/18.
 *
 * @author TerrySolar
 */
@Service("CatalogService")
public class CatalogServiceImpl implements ICatalogService {

    @Resource
    private ICatalogDao catalogDao;

    @Override
    public List<CatalogModel> retrieveCatalog() {
        return this.catalogDao.findAll();
    }

    @Override
    public List<CatalogModel> retrieveCatalog(Long activityId) {
        return this.catalogDao.findByActivityModel_Id(activityId);
    }

    @Override
    public CatalogModel createCatalog(CatalogModel catalogModel) {
        return this.catalogDao.save(catalogModel);
    }

    @Override
    public void deleteCatalog(Long catalogId) {
        this.catalogDao.deleteById(catalogId);
    }
}
