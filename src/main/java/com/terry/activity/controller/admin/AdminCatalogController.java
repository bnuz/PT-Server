package com.terry.activity.controller.admin;

import com.terry.activity.bean.CatalogModel;
import com.terry.activity.bean.message.ResponseMsg;
import com.terry.activity.controller.base.BaseController;
import com.terry.activity.service.ICatalogService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * Create On 18/6/18.
 *
 * @author TerrySolar
 */
@Controller
@RequestMapping(value = "/admin")
@CrossOrigin
public class AdminCatalogController extends BaseController {

    @Resource
    private ICatalogService catalogService;


    @ResponseBody
    @RequestMapping(value = "/catalog/all", method = RequestMethod.GET)
    public ResponseMsg retrieveCatalog() {

        List<CatalogModel> catalogModels = this.catalogService.retrieveCatalog();

        for (CatalogModel catalogModel: catalogModels) {
            catalogModel.getActivityModel().getTitle();
        }

        return responseData(catalogModels);
    }

    @ResponseBody
    @RequestMapping(value = "/catalog", method = RequestMethod.GET)
    public ResponseMsg retrieveCatalog(Long activityId) {

        List<CatalogModel> catalogModels = this.catalogService.retrieveCatalog(activityId);

        for (CatalogModel catalogModel: catalogModels) {
            catalogModel.getActivityModel().getTitle();
        }

        return responseData(catalogModels);
    }

    @ResponseBody
    @RequestMapping(value = "/catalog/create", method = RequestMethod.POST)
    public ResponseMsg createCatalog(CatalogModel catalogModel) {

        catalogModel = this.catalogService.createCatalog(catalogModel);

        return responseData(catalogModel);
    }


    @ResponseBody
    @RequestMapping(value = "/catalog/delete", method = RequestMethod.POST)
    public ResponseMsg createCatalog(Long catalogId) {

        this.catalogService.deleteCatalog(catalogId);

        return responseOK();
    }



}
