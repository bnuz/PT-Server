package com.terry.activity.controller.admin;

import com.terry.activity.bean.ActivityModel;
import com.terry.activity.bean.exception.DataNotExistException;
import com.terry.activity.bean.exception.ParameterException;
import com.terry.activity.bean.message.ResponseMsg;
import com.terry.activity.controller.base.BaseController;
import com.terry.activity.service.IActivityService;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.List;

/**
 * Create On 18/6/17.
 *
 * @author TerrySolar
 */
@Controller
@RequestMapping(value = "/admin")
@CrossOrigin
public class AdminActivityController extends BaseController {

    @Resource
    private IActivityService activityService;


    @ResponseBody
    @RequestMapping(value = "/activity/vote/time", method = RequestMethod.POST)
    public ResponseMsg setActivityVoteTime(Long activityId, String startTime, String endTime) throws ParameterException, DataNotExistException {

        Long start = 0L, end = 0L;

        try {
            start = DateUtils.parseDate(startTime, "yyyy-MM-dd").getTime();
            end = DateUtils.parseDate(endTime, "yyyy-MM-dd").getTime();

            if (start >= end) throw new ParameterException("开始时间需要大于结束时间");
        } catch (ParseException | IllegalArgumentException e) {
            throw new ParameterException("参数错误！");
        }

        this.activityService.setVoteTimeRange(activityId, start, end);

        return responseOK();
    }


    @ResponseBody
    @RequestMapping(value = "/activity", method = RequestMethod.GET)
    public ResponseMsg retrieveActivity(Long activityId) throws DataNotExistException {

        ActivityModel activityModel = this.activityService.getActivity(activityId);

        return responseData(activityModel);
    }

    @ResponseBody
    @RequestMapping(value = "/activity/all", method = RequestMethod.GET)
    public ResponseMsg retrieveActivity() throws DataNotExistException {

        List<ActivityModel> activityModel = this.activityService.getActivity();

        return responseData(activityModel);
    }

    @ResponseBody
    @RequestMapping(value = "/activity/update", method = RequestMethod.POST)
    public ResponseMsg updateActivity(ActivityModel activityModel) throws DataNotExistException {

        activityModel = this.activityService.updateActivity(activityModel);

        return responseData(activityModel);
    }

    @ResponseBody
    @RequestMapping(value = "/activity/create", method = RequestMethod.POST)
    public ResponseMsg addActivity(ActivityModel activityModel) {

        activityModel = this.activityService.addActivity(activityModel);

        return responseData(activityModel);
    }

}
