package com.terry.activity.controller.admin;

import com.terry.activity.bean.EnrollModel;
import com.terry.activity.bean.message.ResponseMsg;
import com.terry.activity.controller.base.BaseController;
import com.terry.activity.service.IEnrollService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 * 管理后台 - 报名管理
 *
 * Create On 18/5/26.
 *
 * @author TerrySolar
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminEnrollController extends BaseController {

    @Resource
    private IEnrollService enrollService;


    @RequestMapping(value = "/enroll/page", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public ResponseMsg retrieveEnroll(Integer page, Integer size) {

        List<EnrollModel> enrolls = this.enrollService.retrieveEnroll(page, size);

        return responseData(enrolls);

    }

}
