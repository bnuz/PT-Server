package com.terry.activity.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.terry.activity.bean.EntryModel;
import com.terry.activity.bean.message.ResponseMsg;
import com.terry.activity.controller.base.BaseController;
import com.terry.activity.dao.IEntryDao;
import com.terry.activity.service.IEntryService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Create On 18/4/26.
 *
 * @author TerrySolar
 */
@Controller
@RequestMapping(value = "/entry")
@CrossOrigin
public class EntryController extends BaseController {

    @Resource
    private IEntryService entryService;


    @RequestMapping(value = "/info", method = RequestMethod.GET)
    @ResponseBody
    public ResponseMsg entryOfActivity(Long activityId, Long catalogId) {
        List<EntryModel> entryModels;

        if (catalogId != null) {
            entryModels = this.entryService.retrieveEntryOfActivityAndCatalog(activityId, catalogId);
        } else {
            entryModels = this.entryService.retrieveAllEntryOfActivity(activityId);
        }

        return responseData(entryModels);
    }


}
