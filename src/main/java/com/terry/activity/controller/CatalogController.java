package com.terry.activity.controller;

import com.terry.activity.bean.CatalogModel;
import com.terry.activity.bean.message.ResponseMsg;
import com.terry.activity.controller.base.BaseController;
import com.terry.activity.service.ICatalogService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * Create On 18/6/19.
 *
 * @author TerrySolar
 */
@Controller
@RequestMapping(value = "/catalog")
@CrossOrigin
public class CatalogController extends BaseController {

    @Resource
    private ICatalogService catalogService;

    @ResponseBody
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResponseMsg retrieveCatalog(Long activityId) {
        List<CatalogModel> catalogModels = this.catalogService.retrieveCatalog(activityId);

        return responseData(catalogModels);
    }


}
