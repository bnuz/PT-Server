package com.terry.activity.controller;

import com.alibaba.fastjson.JSONObject;
import com.terry.activity.bean.message.ResponseMsg;
import com.terry.activity.controller.base.BaseController;
import com.terry.activity.service.IQNService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Create On 18/4/20.
 *
 * @author TerrySolar
 */
@Controller
@CrossOrigin
@RequestMapping(value = "/qiniu")
public class QiniuController extends BaseController {

    @Resource
    private IQNService qnService;


    @RequestMapping(value = "/uploadtoken", method = RequestMethod.GET)
    @ResponseBody
    public ResponseMsg getDefaultUploadToken() {
        JSONObject data = new JSONObject();
        data.put("token", qnService.createUploadToken());
        return responseJson(data);
    }

}
