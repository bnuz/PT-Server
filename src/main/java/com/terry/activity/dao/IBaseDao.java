package com.terry.activity.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * 通用Dao接口，继承自PagingAndSortingRepository extends CrudRepository
 *
 * 已包含基本Crud操作与分页查询
 *
 * Create On 17/9/25.
 *
 * @author TerrySolar
 */
@NoRepositoryBean
public interface IBaseDao<T> extends JpaRepository<T, Long> {

}
