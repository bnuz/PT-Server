package com.terry.activity.dao;

import com.terry.activity.bean.EnrollModel;
import org.springframework.stereotype.Repository;

/**
 * Create On 18/4/22.
 *
 * @author TerrySolar
 */
@Repository
public interface IEnrollDao extends IBaseDao<EnrollModel> {

    EnrollModel findEnrollModelByOpenId(String openId);


}
