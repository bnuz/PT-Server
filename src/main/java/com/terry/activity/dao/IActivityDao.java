package com.terry.activity.dao;

import com.terry.activity.bean.ActivityModel;
import org.springframework.stereotype.Repository;

/**
 * Create On 18/6/15.
 *
 * @author TerrySolar
 */
@Repository
public interface IActivityDao extends IBaseDao<ActivityModel> {
}
