package com.terry.activity.dao;

import com.terry.activity.bean.EntryModel;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Create On 18/4/25.
 *
 * @author TerrySolar
 */
@Repository
public interface IEntryDao extends IBaseDao<EntryModel> {

    /**
     * 根据活动ID查找所有的入选作品
     *
     * @param activityModel_id 活动ID
     * @return List<EntryModel>
     */
    List<EntryModel> findAllByActivityModel_Id(Long activityModel_id);


    /**
     * 根据活动ID和分类ID查找入选作品
     *
     * @param activityModel_id 活动ID
     * @param catalogModel_id 分类ID
     * @return List<EntryModel>
     */
    List<EntryModel> findByActivityModel_IdAndCatalogModel_Id(long activityModel_id, long catalogModel_id);


    /**
     * 根据活动id和报名id查找入围记录
     *
     * @param activityModel_id 活动ID
     * @param enrollModel_id 报名ID
     * @return
     */
    EntryModel findEntryModelByActivityModel_IdAndEnrollModel_Id(long activityModel_id, long enrollModel_id);

}
