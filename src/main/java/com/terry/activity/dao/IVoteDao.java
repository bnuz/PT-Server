package com.terry.activity.dao;

import com.terry.activity.bean.VoteModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * Create On 18/4/25.
 *
 * @author TerrySolar
 */
@Repository
public interface IVoteDao extends IBaseDao<VoteModel> {

    /**
     * 根据openId和活动ID查找投票记录
     *
     * @param openId 微信openId
     * @param activityModel_id 活动ID
     * @return VoteModel
     */
    VoteModel findVoteModelByOpenIdAndActivityModel_Id(String openId, long activityModel_id);


    /**
     * 根据openid、活动ID、分类ID
     *
     * @param openId 微信openId
     * @param activityModel_id 活动ID
     * @param catalogModel_id 分类ID
     * @return VoteModel
     */
    VoteModel findByOpenIdAndActivityModel_IdAndCatalogModel_Id(String openId, long activityModel_id, long catalogModel_id);


    /**
     * 根据entryId统计票数
     *
     * @param entryModel_id entryId
     * @return 该entryId的票数
     */
    Integer countByEntryModel_Id(Long entryModel_id);



}
