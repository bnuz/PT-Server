package com.terry.activity.dao;

import com.terry.activity.bean.CatalogModel;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Create On 18/6/18.
 *
 * @author TerrySolar
 */
@Repository
public interface ICatalogDao extends IBaseDao<CatalogModel> {

    List<CatalogModel> findByActivityModel_Id(long activityModel_id);

}
